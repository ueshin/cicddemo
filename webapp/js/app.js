/* global window, Vue */

var taskForm = {
	template: '#template-task-form',
	props: [
		'task',
		'on-submit',
	],
	methods: {
		form_submit: function(event) {
			if (!this.task.name) {
				return;
			}
			var userid = location.pathname.split("/")[2] ;
			var apiurl = '/api/todocreate/' + userid;
			axios.post( apiurl , 
				this.task
			, {
					headers: { 'Content-Type': 'application/json' }
			})
			.then( response => {
				app.reload()
			})
			.catch(error => {
				console.log(error);
				return
			});

			this.onSubmit(event, this.task);
		},
	},
};

var taskItem = {
	template: '#template-task-item',
	props: [
		'task',
	],
	methods: {
		edit_click: function(event) {
			// update if not canceled
			var newName = window.prompt('Task Name', this.task.name);
			var userid = location.pathname.split("/")[2] ;
			var apiurl = '/api/todoupdate/' + userid;
			axios.post( apiurl , 
				this.task
			, {
					headers: { 'Content-Type': 'application/json' }
			})
			.catch(error => {
				console.log(error);
				return
			});
			if (typeof newName === 'string') {
				this.task.name = newName;
			}
		},
	},
};

window.app = new Vue({
	el: '#app',
	components: {
		taskForm: taskForm,
		taskItem: taskItem,
	},
	data: {
		newTask: { finished: false, name: '' },
		tasks: [{name: "init test", finished: false}] 
	},
	methods: {
		newTask_submit: function(event) {
			this.tasks.push(this.newTask);
			this.newTask = { finished: false, name: '' };
		},

		delete_click: function(event) {
			this.tasks = this.tasks.filter(v=>!v.finished);
			var userid = location.pathname.split("/")[2] ;
			var endpoint = '/api/tododelete/' + userid;
			axios.post( endpoint , 
				this.tasks
			, {
					headers: { 'Content-Type': 'application/json' }
			})
			.then( response => {
				app.reload()
			})
			.catch(error => {
				console.log(error);
				return
			});
		},

		reload: function(event){
			var userid = location.pathname.split("/")[2] ;
			var endpoint = '/api/todolist/' + userid;
			axios.get(endpoint)
			.then((response) => {
				this.tasks = response.data;
			});
		},
	},
	created(){
		this.reload();
	},
});
