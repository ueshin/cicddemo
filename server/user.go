package server

import (
	"fmt"

	"github.com/satori/go.uuid"
)

type User struct {
	Id    int
	Name  string
	Todos []Todo
}

var iduser map[int]User = map[int]User{}

func GetAndCreateUser(userId int) User {
	user, ok := iduser[userId]
	if !ok {
		iduser[userId] = User{Id: userId}
		user = iduser[userId]
	}
	return user
}

func (u *User) GetTodos() []Todo {
	return u.Todos
}

func (u *User) CreateTodo(t Todo) User {
	ui := uuid.NewV4()
	uistr := fmt.Sprintf("%s", ui)
	t.Id = uistr
	u.Todos = append(u.Todos, t)
	iduser[u.Id] = *u
	return iduser[u.Id]
}

func (u *User) SetTodos(ts []Todo) {
	u.Todos = ts
	iduser[u.Id] = *u
}

func (u *User) UpdateTodo(t Todo) error {
	for i, ntodo := range u.Todos {
		if t.Id == ntodo.Id {
			u.Todos[i] = t
			return nil
		}
	}
	return fmt.Errorf("Can't find Todo to update User Todo")
}

func (u *User) DeleteTodo(t Todo) error {
	for i, ntodo := range u.Todos {
		if t.Id == ntodo.Id {
			u.Todos = append(u.Todos[:i], u.Todos[i+1:]...)
			return nil
		}
	}
	return fmt.Errorf("Can't find Todo to delete user Todo")
}

func (u *User) LenTodos() int {
	return len(u.Todos)
}
