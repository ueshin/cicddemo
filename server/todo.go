package server

// Todo is...
type Todo struct {
	Id       string `json:"uuid"`
	Name     string `json:"name"`
	Finished bool   `json:"finished"`
}
