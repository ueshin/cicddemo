package server

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

// TodoIndex returns todo index
func TodoList(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	userIdStr := ps.ByName("userId")
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		panic(err)
	}

	user := GetAndCreateUser(userId)
	if user.LenTodos() == 0 {
		t1 := Todo{Name: "内田さん説明"}
		user.CreateTodo(t1)
		json.NewEncoder(w).Encode(user.GetTodos())
	} else {
		json.NewEncoder(w).Encode(user.GetTodos())
	}
}

// TodoCreate to create todo
func TodoCreate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	userIdStr := ps.ByName("userId")
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		panic(err)
	}
	user := GetAndCreateUser(userId)

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(400)
		log.Fatal(err)
	}
	var newtodo Todo
	err = json.Unmarshal(body, &newtodo)
	if err != nil {
		w.WriteHeader(400)
		log.Fatal(err)
	}
	user = user.CreateTodo(newtodo)
	json.NewEncoder(w).Encode(user)
	w.WriteHeader(200)
}

func TodoUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	userIdStr := ps.ByName("userId")
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		panic(err)
	}
	user := GetAndCreateUser(userId)

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(400)
		log.Fatal(err)
	}
	var targetTodo Todo
	err = json.Unmarshal(body, &targetTodo)
	if err != nil {
		w.WriteHeader(400)
		log.Fatal(err)
	}
	user.UpdateTodo(targetTodo)
	w.WriteHeader(200)
}

func AllTodosUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	userIdStr := ps.ByName("userId")
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		panic(err)
	}
	user := GetAndCreateUser(userId)

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(400)
		log.Fatal(err)
	}
	var targetTodos []Todo
	err = json.Unmarshal(body, &targetTodos)
	if err != nil {
		w.WriteHeader(400)
		log.Fatal(err)
	}
	user.SetTodos(targetTodos)
	w.WriteHeader(200)
}
