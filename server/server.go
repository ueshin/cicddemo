package server

import (
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/julienschmidt/httprouter"
)

func RunServer() {

	router := httprouter.New()
	router.GET("/api/todolist/:userId", Logging(TodoList, "todoindex"))
	router.POST("/api/todocreate/:userId", Logging(TodoCreate, "todocreate"))
	router.POST("/api/todoupdate/:userId", Logging(TodoUpdate, "todoupdate"))
	router.POST("/api/tododelete/:userId", Logging(AllTodosUpdate, "todoupdate"))
	pwd, _ := os.Getwd()
	htmlpath := filepath.Join(pwd, "webapp/html")
	jspath := filepath.Join(pwd, "webapp/js")
	csspath := filepath.Join(pwd, "webapp/css")
	router.ServeFiles("/app/:userId/*filepath", http.Dir(htmlpath))
	router.ServeFiles("/static/js/*filepath", http.Dir(jspath))
	router.ServeFiles("/static/css/*filepath", http.Dir(csspath))

	log.Fatal(http.ListenAndServe(":8080", router))
}
