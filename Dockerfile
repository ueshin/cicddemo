FROM busybox:latest
WORKDIR /app
COPY dist/runserver /app/
COPY webapp /app/webapp/
CMD ["/app/runserver"]
